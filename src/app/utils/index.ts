import * as moment from 'moment';

export const GetPrettyDate = (date: Date): string => {
	return moment(date).format('L');
}
