export interface IFormStepperInterface {
	fullName: IFieldFormInterface,
	birthday: IFieldFormInterface,
	sex: IFieldFormInterface,
	snils: IFieldFormInterface,
}

interface IFieldFormInterface {
	label: string,
	matLabel: string,
	placeholder: string,
}
