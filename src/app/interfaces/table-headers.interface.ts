export interface ITableHeadersInterface {
	fullName: string,
	birthday: string,
	sex: string,
	snils: string,
};
