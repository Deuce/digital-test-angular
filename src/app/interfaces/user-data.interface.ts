export interface IUserDataInterface {
	id?: number,
	fullName: string,
	birthday: Date,
	sex: string,
	snils: string,
}
