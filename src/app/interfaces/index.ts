export * from './user-data.interface';

export * from './sex-options.interface';
export * from './table-headers.interface';
export * from './form-stepper.interface';
