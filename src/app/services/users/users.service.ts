import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { IUserDataInterface } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  usersData: BehaviorSubject<IUserDataInterface[]>;

  constructor()  {
    this.usersData = new BehaviorSubject<IUserDataInterface[]>([]);
 }

  getUsersData(): Observable<IUserDataInterface[]> {
    return this.usersData.asObservable();
  }

  addItem(userInfo: IUserDataInterface): void {
    this.usersData.next([...this.usersData.getValue(), userInfo]);
  }
}
