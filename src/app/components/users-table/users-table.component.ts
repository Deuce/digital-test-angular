import { Component, OnInit } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

import { UsersService } from '../../services';
import { GetPrettyDate } from '../../utils';
import {
  IUserDataInterface,
  ITableHeadersInterface
} from '../../interfaces';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  usersData: IUserDataInterface[];
  displayedColumns: string[];
  tableHeaders: ITableHeadersInterface;
  getPrettyDate = GetPrettyDate;

  data: any;
  dataSource: MatTableDataSource<Element>;
  selectionData = new SelectionModel<Element>(true, []);

  constructor(private usersService: UsersService) {
    this.displayedColumns = ['select', 'fullName', 'birthday', 'sex', 'snils'];
    this.tableHeaders = {
      fullName: 'фио',
      birthday: 'дата рождения',
      sex: 'пол',
      snils: 'снилс',
    };
  }

  ngOnInit() {
    this.usersService.getUsersData().subscribe((users) => {
      this.usersData = users;
      this.data = Object.assign(this.usersData);
      this.dataSource = new MatTableDataSource<Element>(Object.assign(this.data));
    });
  }

  isCheckForAllSelect (): boolean {
    const countSelected = this.selectionData.selected.length;
    const countRows = this.dataSource.data.length;

    return countSelected === countRows;
  };

  selectAndClearToogle (): void {
    this.isCheckForAllSelect() ?
      this.selectionData.clear() :
      this.dataSource.data.forEach((row) => this.selectionData.select(row));
  }

  removeSelectedRows (): void {
    this.selectionData.selected.forEach((item) => {
      const index: number = this.data.findIndex((d: Element) => d === item);

      this.data.splice(index, 1)
      this.dataSource = new MatTableDataSource<Element>(this.data);
    });

    this.selectionData = new SelectionModel<Element>(true, []);
  }
}
