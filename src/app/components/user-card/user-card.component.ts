import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  ValidationErrors
} from '@angular/forms';

import { MatStepper } from '@angular/material/stepper';

import { UsersService } from '../../services';
import {
  IUserDataInterface,
  ISexOptionsInterface,
  IFormStepperInterface
} from '../../interfaces';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  fullNameFormGroup: FormGroup;
  birthdayFormGroup: FormGroup;
  sexFormGroup: FormGroup;
  snilsFormGroup: FormGroup;

  minDate: Date;
  maxDate: Date;

  sexOptions: ISexOptionsInterface[];
  formInformation: IFormStepperInterface;

  constructor(private formBuilder: FormBuilder, private usersService: UsersService) {
    const currentYear: number = new Date().getFullYear();

    const minLimitYear: number = 80;
    const minLimitMonth: number = 0;
    const minLimitDay: number = 1;

    this.minDate = new Date(currentYear - minLimitYear, minLimitMonth, minLimitDay);
    this.maxDate = new Date();

    this.sexOptions = [
      {
        id: 1,
        view: 'Мужской',
      },
      {
        id: 2,
        view: 'Женский',
      }
    ];

    this.formInformation = {
      fullName: {
        label: 'фио',
        matLabel: 'введите фио',
        placeholder: 'фамилия имя отчество',
      },
      birthday: {
        label: 'дата рождения',
        matLabel: 'введите дату рождения',
        placeholder: 'x/x/xxxx',
      },
      sex: {
        label: 'пол',
        matLabel: 'выбрите ваш пол',
        placeholder: 'мужской',
      },
      snils: {
        label: 'снилс',
        matLabel: 'введите снилс',
        placeholder: 'xxxxxxxxxxx',
      }
    }
  }

  ngOnInit(): void {
    const emptyString: string = '';

    this.fullNameFormGroup = this.formBuilder.group({
      fullNameCtrl: [emptyString, Validators.required]
    });
    this.birthdayFormGroup = this.formBuilder.group({
      birthdayCtrl: [emptyString, Validators.required]
    });
    this.sexFormGroup = this.formBuilder.group({
      sexCtrl: [emptyString, Validators.required]
    });
    this.snilsFormGroup = this.formBuilder.group({
      snilsCtrl: [emptyString]
    });

    this.snilsFormGroup.setValidators(this.validateSnils());
  }

  validateSnils(): ValidatorFn {
    return (snilsGroup: FormGroup): ValidationErrors => {
      const snilsValidateCtrl = snilsGroup.controls['snilsCtrl'];
      const snils: string = snilsValidateCtrl.value;

      const maxLengthOfSnils: number = 11;

      const summOfMiltipleSnils = (snils: string) => {
        if (snils && snils.length === 0) {
          return 0;
        }

        const maxNumber: number = 9;

        const iter = (counter: number, acc: number) => {
          if (counter === maxNumber) {
            return acc;
          }

          const newAcc = parseInt(snils[counter]) * (9 - counter);

          return iter(counter + 1, acc + newAcc);
        };

        return iter(0, 0);
      }

      if (snils && snils.length === 0) {
        snilsValidateCtrl.setErrors({ isEmpty: true });
      } else if (/[^0-9]/.test(snils)) {
        snilsValidateCtrl.setErrors({ isNotAllNumbers: true });
      } else if (snils && snils.length !== maxLengthOfSnils) {
        snilsValidateCtrl.setErrors({ isNotRightLength: true });
      } else {

        const sum = summOfMiltipleSnils(snils);
        let checkDigit = 0;

        if (sum < 100) {
          checkDigit = sum;
        } else if (sum > 101) {
          checkDigit = sum % 101;

        if (checkDigit === 100) {
          checkDigit = 0;
        }
      }

      if (checkDigit !== parseInt(snils.slice(-2))) {
        snilsValidateCtrl.setErrors({ isNotRightControlNumber: true });
      }
    }

    return;
    }
  }

  onConfirm (stepper: MatStepper): void {
    const newUserData: IUserDataInterface = {
      fullName: this.fullNameFormGroup.value.fullNameCtrl,
      birthday: this.birthdayFormGroup.value.birthdayCtrl,
      sex: this.sexFormGroup.value.sexCtrl,
      snils: this.snilsFormGroup.value.snilsCtrl,
    }

    const isFieldsValid = this.fullNameFormGroup.valid
      && this.birthdayFormGroup.valid
      && this.sexFormGroup.valid
      && this.snilsFormGroup.valid;

    if (isFieldsValid) {
      this.usersService.addItem(newUserData);
      stepper.reset();
    }
  };
}
